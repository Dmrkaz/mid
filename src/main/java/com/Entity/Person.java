import  javax.*;
        lombok.*;



@Data
@Entity
@Table(name="Person")
public class Person{
    private int id;
    private string firstname;
    private string lastname;
    private string city;
    private string phone;
    private string telegram;
}