drop table if exists Person CASCADE;
CREATE TABLE Person (
                                 id BIGSERIAL NOT NULL,
                                 firstname VARCHAR(128),
                                 lastname VARCHAR(255),
                                 city VARCHAR(255),
                                 phone VARCHAR(12),
	                             telegram VARCHAR(255),
                                 PRIMARY KEY (id)
);